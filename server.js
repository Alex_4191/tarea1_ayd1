const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/suma', (req, res) => {
  const {valor1, valor2} = req.body;
  
  if((valor1 && valor2) != null){
    return res.status(200).send(`El resultado de la suma es:  ${valor1+valor2}`)
  }
  return res.status(500).send("Datos Ingresadod Erroneamente")
  
});

app.post('/resta', (req, res) => {
  const {valor1, valor2} = req.body;
  
  if((valor1 && valor2) != null){
    return res.status(200).send(`El resultado de la suma es:  ${valor1-valor2}`)
  }
  return res.status(500).send("Datos Ingresadod Erroneamente")
  
});

app.post('/multi', (req, res) => {
  const {valor1, valor2} = req.body;
  
  if((valor1 && valor2) != null){
    return res.status(200).send(`El resultado de la suma es:  ${valor1*valor2}`)
  }
  return res.status(500).send("Datos Ingresadod Erroneamente")
  
});

app.listen(port, () => console.log(`Server is listening on port ${port}!`));